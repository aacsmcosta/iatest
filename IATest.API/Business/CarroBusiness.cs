using IATest.API.Models;
using IATest.API.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace IATest.API.Business
{
    public class CarroBusiness : ICarroBusiness
    {
        private readonly ICarroRepository _carroRepository;

        public CarroBusiness(ICarroRepository carroRepository)
        {
            _carroRepository = carroRepository;
        }

        public IEnumerable<Carro> GetCarros()
        {
            return _carroRepository.GetCarros();
        }

        public IEnumerable<Carro> GetCarrosByModelo(String modelo)
        {
            return GetCarros().Where(c => c.Modelo == modelo);
        }

        public IEnumerable<T> GetCarrosByModelo<T>(String modelo) where T : Carro
        {
            return _carroRepository
                .GetCarros<T>()
                .Where(c => c.Modelo == modelo);
        }

        public IEnumerable<ModeloAutonomia> GetAutonomias()
        {
            return GetCarros()
                .Select(c => new ModeloAutonomia(c.Modelo, c.Autonomia));
        }

        public void RefuelCarroByModelo(String modelo)
        {
            var carros = GetCarrosByModelo(modelo);

            foreach(var carro in carros)
            {
                carro.Abastecer();
            }

            Save();
        }

        public void ActivateSpecialFeatureByModelo<T>(String modelo) where T : Carro, ISpecialFeature
        {
            foreach(var carro in GetCarrosByModelo<T>(modelo))
            {
                carro.ActivateSpecialFeature();
            }

            Save();
        }

        public IEnumerable<ModeloAutonomia> GetHighestAutonomia()
        {
            var carros = GetCarros();
            
            var maxAutonomia = carros.Max(c => c.Autonomia);
            
            var result = carros
                .Where(c => c.Autonomia == maxAutonomia)
                .Select(c => new ModeloAutonomia(c.Modelo, c.Autonomia));

            return result;
        }

        public IEnumerable<Carro> GetFasterCarroByDistance(Double distance)
        {
            var carros = GetCarros();
            
            var bestTempoPercurso = carros.Max(c => c.CalcularTempoPercurso(distance));

            var result = carros
                .Where(c => !Double.IsNaN(c.CalcularTempoPercurso(distance)) 
                            && c.CalcularTempoPercurso(distance) == bestTempoPercurso);
            
            return result;
        }

        private void Save()
        {
            _carroRepository.Save();
        }

        // public void ActivateEconomicoByModelo(String modelo)
        // {
        //     foreach(var carro in GetCarrosEconomicoByModelo(modelo))
        //     {
        //         carro.ActivateSpecialFeature();
        //     }

        //     _carroRepository.Save();
        // }

        // protected IEnumerable<CarroTurboDecorator> GetCarrosTurboByModelo(String modelo)
        // {
        //     return GetCarrosByTipo<CarroTurboDecorator>()
        //         .Where(c => c.Modelo == modelo);
        // }

        // protected IEnumerable<CarroEconomicoDecorator> GetCarrosEconomicoByModelo(String modelo)
        // {
        //     return GetCarrosByTipo<CarroEconomicoDecorator>()
        //         .Where(c => c.Modelo == modelo);
        // }

        // private IEnumerable<T> GetCarrosByTipo<T>()
        // {
        //     return _carroRepository.GetCarros<T>();
        // }
    }
}
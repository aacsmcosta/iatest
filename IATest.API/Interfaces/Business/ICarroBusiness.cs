using IATest.API.Models;
using IATest.API.Interfaces;

namespace IATest.API.Interfaces
{
    public interface ICarroBusiness
    {
        IEnumerable<Carro> GetCarros();

        void RefuelCarroByModelo(String modelo);
        
        IEnumerable<ModeloAutonomia> GetAutonomias();

        void ActivateSpecialFeatureByModelo<T>(String modelo) where T : Carro, ISpecialFeature;

        IEnumerable<ModeloAutonomia> GetHighestAutonomia();

        IEnumerable<Carro> GetFasterCarroByDistance(Double distance);

    }
}
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IATest.API.Interfaces
{
    public interface ICarro
    {
        void Abastecer();

        Double CalcularTempoPercurso(Double distancia);
    }
}
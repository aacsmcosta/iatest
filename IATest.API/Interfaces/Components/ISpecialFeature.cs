namespace IATest.API.Interfaces
{
    public interface ISpecialFeature
    {
        void ActivateSpecialFeature();
    }
}
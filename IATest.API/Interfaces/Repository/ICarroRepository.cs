using IATest.API.Models;
using IATest.API.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace IATest.API.Interfaces
{
    public interface ICarroRepository
    {
        IEnumerable<Carro> GetCarros();
        IEnumerable<T> GetCarros<T>();
        void Save();
    }
}
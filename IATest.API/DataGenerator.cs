using IATest.API.Models;
using Microsoft.EntityFrameworkCore;

public class DataGenerator
{
    public static void Initialize(IServiceProvider serviceProvider)
    {
        using (var context = new ApiContext(serviceProvider.GetRequiredService<DbContextOptions<ApiContext>>()))
        {
            List<Carro> carros = new List<Carro>
            {
                new CarroBasico
                {
                    Id = 1,
                    Modelo = "Honda Civic",
                    CapacidadeTanque = 50,
                    KmPorLitro = 12,
                    Preco = 73540,
                    QtsLitrosNoTanque = 45,
                    VelocidadeMedia = 65
                },
                new CarroBasico
                {
                    Id = 2,
                    Modelo = "Chevrolet Onix",
                    CapacidadeTanque = 60,
                    KmPorLitro = 11,
                    Preco = 121750,
                    QtsLitrosNoTanque = 12,
                    VelocidadeMedia = 60
                },
                new CarroBasico
                {
                    Id = 3,
                    Modelo = "Hyundai HB20",
                    CapacidadeTanque = 55,
                    KmPorLitro = 14,
                    Preco = 83250,
                    QtsLitrosNoTanque = 33,
                    VelocidadeMedia = 58
                },
                new CarroBasico
                {
                    Id = 4,
                    Modelo = "Renault Logan",
                    CapacidadeTanque = 50,
                    KmPorLitro = 13.5,
                    Preco = 99260,
                    QtsLitrosNoTanque = 48,
                    VelocidadeMedia = 63
                },
                new CarroBasico
                {
                    Id = 5,
                    Modelo = "Renault Logan",
                    CapacidadeTanque = 50,
                    KmPorLitro = 13.5,
                    Preco = 99260,
                    QtsLitrosNoTanque = 20,
                    VelocidadeMedia = 63
                },
                new CarroBasico
                {
                    Id = 6,
                    Modelo = "Hyundai HB20",
                    CapacidadeTanque = 55,
                    KmPorLitro = 14,
                    Preco = 83250,
                    QtsLitrosNoTanque = 50,
                    VelocidadeMedia = 58
                }
            };

            Carro carroBasico = new CarroBasico
            {
                Id = 7,
                Modelo = "Renault Kwid",
                CapacidadeTanque = 50,
                KmPorLitro = 15.5,
                Preco = 50540,
                QtsLitrosNoTanque = 29,
                VelocidadeMedia = 68
            };

            Carro carroEconomico = new CarroEconomicoDecorator(carroBasico);
            carros.Add(carroEconomico);

            Carro carroBasico2 = new CarroBasico
            {
                Id = 8,
                Modelo = "Audi A3",
                CapacidadeTanque = 50,
                KmPorLitro = 9,
                Preco = 127340,
                QtsLitrosNoTanque = 42,
                VelocidadeMedia = 80,
            };

            Carro carroTurbo = new CarroTurboDecorator(carroBasico2);
            carros.Add(carroTurbo);

            context.Carros.AddRange(carros);

            context.SaveChanges();
        }
    }
}
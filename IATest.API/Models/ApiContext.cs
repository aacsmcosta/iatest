using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using IATest.API.Interfaces;

namespace IATest.API.Models
{
    public class ApiContext : DbContext
    {
        public DbSet<Carro> Carros { get; set; } = null!;
        
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CarroBasico>().ToTable("CarrosBasicos");
            modelBuilder.Entity<CarroTurboDecorator>().ToTable("CarrosTurbos");
            modelBuilder.Entity<CarroEconomicoDecorator>().ToTable("CarrosEconomicos");
        }

    }
}
using System.ComponentModel.DataAnnotations.Schema;
using IATest.API.Interfaces;

namespace IATest.API.Models
{
    public class CarroTurboDecorator : Carro, ISpecialFeature
    {
        private readonly Carro _carro;

        public CarroTurboDecorator(Carro carro)
        {
            _carro = carro ?? throw new ArgumentNullException(nameof(carro));
        }

        public override void Abastecer()
        {
            _carro.Abastecer();
        }

        public override Double CalcularTempoPercurso(Double distancia)
        {
            return _carro.CalcularTempoPercurso(distancia);
        }

        public void ActivateSpecialFeature()
        {
            this.AtivarModoTurbo();
        }

        private void AtivarModoTurbo()
        {
            this.KmPorLitro = this.KmPorLitro/1.3;
            this.VelocidadeMedia += this.VelocidadeMedia * 0.15;
            this.Preco += this.Preco * 0.1;
        }
    }
}
using System.ComponentModel.DataAnnotations.Schema;
using IATest.API.Interfaces;

namespace IATest.API.Models
{
    public class CarroEconomicoDecorator : Carro, ISpecialFeature
    {
        public new Double Autonomia
        {
            get { return KmPorLitro * QtsLitrosNoTanque * _autonomiaMod; }
        }

        [Column]
        private Double _autonomiaMod = 1;

        private readonly Carro _carro;

        public CarroEconomicoDecorator(Carro carro)
        {
            _carro = carro ?? throw new ArgumentNullException(nameof(carro));
        }


        public override void Abastecer()
        {
            _carro.Abastecer();
        }

        public override Double CalcularTempoPercurso(Double distancia)
        {
            if (Autonomia < distancia)
            {
                return Double.NaN;
            }

            if (VelocidadeMedia.Equals(0d))
        {
                throw new DivideByZeroException(nameof(VelocidadeMedia));
            }

            return distancia / VelocidadeMedia;
        }

        public void ActivateSpecialFeature()
        {
            this.AtivarModoEconomico();
        }        

        private void AtivarModoEconomico()
        {
            this.KmPorLitro = this.KmPorLitro/0.9;
            this.Preco += this.Preco * 0.05;
            this._autonomiaMod = 1.05;
        }
    }
}
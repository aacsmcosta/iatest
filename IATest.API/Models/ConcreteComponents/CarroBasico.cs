using IATest.API.Interfaces;

namespace IATest.API.Models
{
    public class CarroBasico : Carro
    {
        public override void Abastecer()
        {
            QtsLitrosNoTanque = CapacidadeTanque;
        }

        public override Double CalcularTempoPercurso(Double distancia)
        {
            if (Autonomia < distancia)
            {
                return Double.NaN;
            }

            if (VelocidadeMedia.Equals(0d))
            {
                throw new DivideByZeroException(nameof(VelocidadeMedia));
            }

            return distancia / VelocidadeMedia;
        }
    }
}
namespace IATest.API.Models
{
    public class ModeloAutonomia
    {
        public String Modelo { get; set; }
        public Double Autonomia { get; set; }

        public ModeloAutonomia(String modelo, Double autonomia)
        {
            Modelo = modelo;
            Autonomia = autonomia;
        }
    }
}
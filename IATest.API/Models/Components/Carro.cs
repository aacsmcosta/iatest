using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using IATest.API.Interfaces;

namespace IATest.API.Models
{
    public abstract class Carro : ICarro
    {
        public Int32 Id { get; set; }
        public String Modelo { get; set; }
        public Double Preco { get; set; }
        public Double CapacidadeTanque { get; set; }
        public Double QtsLitrosNoTanque { get; set; }
        public Double KmPorLitro { get; set; }
        public Double VelocidadeMedia { get; set; }

        public Double Autonomia
        {
            get { return KmPorLitro * QtsLitrosNoTanque; }
        }
        
        public abstract void Abastecer();

        public abstract Double CalcularTempoPercurso(Double distancia);

    }
}
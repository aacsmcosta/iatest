using Microsoft.EntityFrameworkCore;
using IATest.API;
using IATest.API.Models;
using IATest.API.Interfaces;
using IATest.API.Repository;
using IATest.API.Business;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<ApiContext>(opt => 
{
    opt.EnableSensitiveDataLogging(); opt.UseInMemoryDatabase("DatabaseList");
}, ServiceLifetime.Singleton);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddScoped<ICarroRepository, CarroRepository>();
builder.Services.AddScoped<ICarroBusiness, CarroBusiness>();
builder.Services.AddSingleton<ICarro>(serviceProvider => new CarroEconomicoDecorator(new CarroBasico()));
// builder.Services.AddScoped<Carro>(serviceProvider => new CarroEconomicoDecorator(new CarroBasico()));
// builder.Services.AddScoped<Carro>(serviceProvider => new CarroTurboDecorator(new CarroBasico()));
//builder.Services.AddSingleton<Carro>(serviceProvider => new CarroConcreteDecorator(new CarroBasico()));

//builder.Services.AddSwaggerGen();

var app = builder.Build();

DataGenerator.Initialize(app.Services);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    //app.UseSwagger();
    //app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

using IATest.API.Models;
using IATest.API.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace IATest.API.Repository
{
    public class CarroRepository : ICarroRepository
    {
        private readonly ApiContext _context;

        public CarroRepository(ApiContext context)
        {
            _context = context;
        }

        public IEnumerable<Carro> GetCarros()
        {
            return GetRepository().OrderBy(c => c.Id).ToList();
        }

        public IEnumerable<T> GetCarros<T>()
        {
            return GetRepository()
                .OfType<T>()
                .ToList();
        }
        
        public void Save()
        {
            _context.SaveChanges();
        }

        public DbSet<Carro> GetRepository()
        {
            return _context.Carros;
        }
    }
}
#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IATest.API.Models;
using IATest.API.Interfaces;

namespace IATest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarroController : ControllerBase
    {
        private readonly ICarroBusiness carroBusiness;

        public CarroController(ICarroBusiness carroBusiness)
        {
            this.carroBusiness = carroBusiness;
        }

        // GET: api/Carro
        [HttpGet]
        public IEnumerable<Carro> GetCarroItems()
        {
            return carroBusiness.GetCarros();
        }

        [HttpPatch("RefuelCarroByModelo")]
        public void RefuelCarroByModelo([FromBody] String modelo)
        {
            carroBusiness.RefuelCarroByModelo(modelo);
        }

        [HttpGet("GetAutonomias")]
        public IEnumerable<ModeloAutonomia> GetAutonomias()
        {
            return carroBusiness.GetAutonomias();
        }

        [HttpPatch("ActivateTurboByModelo")]
        public void ActivateTurboByModelo([FromBody] String modelo)
        {
            carroBusiness.ActivateSpecialFeatureByModelo<CarroTurboDecorator>(modelo);
        }

        [HttpPatch("ActivateEconomicoByModelo")]
        public void ActivateEconomicoByModelo([FromBody] String modelo)
        {
            carroBusiness.ActivateSpecialFeatureByModelo<CarroEconomicoDecorator>(modelo);
        }

        [HttpGet("GetHighestAutonomia")]
        public IEnumerable<ModeloAutonomia> GetHighestAutonomia()
        {
            return carroBusiness.GetHighestAutonomia();
        }

        [HttpGet("GetFasterCarroByDistance")]
        public IEnumerable<Carro> GetFasterCarroByDistance([FromBody] Double distance)
        {
            return carroBusiness.GetFasterCarroByDistance(distance);
        }
    }
}
